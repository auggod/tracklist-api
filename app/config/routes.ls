_ = require 'lodash'

module.exports = (app, passport) ->

  #Require all the things
  require! {
    '../api/tracks'
    '../api/events'
    '../api/artists'
    '../api/search'
    '../api/labels'
    '../api/venues'
    '../api/users'
  }

  images = require('../api/images')

  #Auth middlewares
  auth = require './middlewares/authorization'

  #Allowed origins configuration
  app.all '*', (req, res, next) ->
    return next! if not req.get 'Origin'
    allowedOrigins = [
      'http://localhost:3000'
      'https://tracklist'
      'http://tracklist'
      'http://auggod.io'
      'http://trkl.st'
      'http://godiscal.com'
      'http://tracklistapp.com'
      'http://dev.tracklistapp.com'
      'https://dev.tracklistapp.com'
    ]
    origin = req.get 'Origin'
    if _.contains(allowedOrigins, origin)
      res.header 'Access-Control-Allow-Origin', origin
      res.header 'Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS'
      res.header 'Access-Control-Allow-Headers', 'Content-Type, Accept, Cache-Control, Pragma, Origin, Authorization, X-Tracklist-User-Id, X-Tracklist-Session-Id, X-Requested-With'
    else
      res.header 'Access-Control-Allow-Origin', origin
      res.header 'Access-Control-Allow-Methods', 'GET'
    if 'OPTIONS' is req.method then return res.sendStatus 200
    next!
    return 

  rest = (nameController, controller) ->
    app.get "/#nameController"        controller.list   if controller.list
    app.get "/#nameController/:id"    controller.show   if controller.show
    app.post "/#nameController"       controller.create if controller.create
    app.put "/#nameController/:id"    controller.update if controller.update
    app.delete "/#nameController/:id" controller.remove if controller.remove

  #Auth, users
  app.get '/logout', users.logout
  app.post '/users', users.create
  app.get '/user', users.auth
  app.get '/user/:userId', events.findUser
  app.get '/users/:username', users.show
  app.post '/users/session', (passport.authenticate 'local'), users.auth
  app.put '/users/:username', users.update
  app.delete '/users/:username', users.remove
  app.get '/users/pages/:page', users.list

  #Events TODO pagination optional
  app.get '/events/:id', events.show
  app.get '/events/:q?/:tags?/:active?/:limit?/:archive?/:username?', events.list
  
  app.get '/events/user/:username', events.findUserEvents
  app.post '/events', events.create
  app.put '/events/:id/images', events.addImages
  app.put '/events/:id', events.update
  app.delete '/events/:id', events.destroy

  #Tracks
  app.get '/tracks/:event?', tracks.list
  app.get '/tracks/:id', tracks.show
  app.post '/tracks/:event?', tracks.create
  app.delete '/tracks/:id/:event?', tracks.destroy

  #Labels
  app.get '/labels', labels.list
  app.get '/labels/:permalink', labels.show
  app.post '/labels', labels.create
  app.put '/labels/:permalink', labels.update
  app.delete '/labels/:permalink', labels.remove

  #Tags
  rest "tags" require('../api/tags')

  #Artists
  app.get '/artists/:id', artists.show
  app.get '/artists/:q?', artists.list
  app.post '/artists', artists.create
  app.put '/artists/:permalink', artists.update

  #Upload
  app.post '/upload/', images.upload

  #Venues
  app.get '/venues/:q?', venues.list
  app.get '/venues/:permalink', venues.show

  app.get '*', (req, res) ->
    res.send 'Tracklist API is running'
