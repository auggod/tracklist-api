require! {
  mongoose  
  'mongoose-auto-increment': autoIncrement
  'mongoose-validator': validate
  '../lib/slug'
}

Schema = mongoose.Schema
autoIncrement.initialize mongoose

#Validations

uriValidator = [ validate(
  validator: 'isLength'
  arguments: [
    20
    100
  ]
  message: 'Title should be between 3 and 50 characters') ]

TrackSchema = new Schema(
  id: type: Number
  events: [ { _event:
    type: Number
    ref: 'Event' } ]
  source:
    type: String
    default: 'Soundcloud'
  uri:
    type: String
  url:
    type: String
  createdAt:
    type: Date
    default: Date.now)

TrackSchema.plugin autoIncrement.plugin,
  model: 'Track'
  field: 'id'
  startAt: 1
  incrementBy: 1

module.exports = mongoose.model('Track', TrackSchema)
